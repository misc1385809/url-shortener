﻿using ShortUrl;

const string ShortUrlDomain = "https://shorturl.com";
var shortener = new UrlShortener();

try
{
    string originalUrl = "https://example.com/page";
    string shortUrl = shortener.ShortenUrl(originalUrl);
    Console.WriteLine($"Shortened URL: {ShortUrlDomain}/{shortUrl}");

    string expandedUrl = shortener.ExpandUrl(shortUrl);
    Console.WriteLine($"Expanded URL: {expandedUrl}");
}
catch (Exception ex)
{
    Console.WriteLine($"Something unexpected happened: {ex.Message}");
}

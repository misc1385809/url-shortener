# URL Shortener
Shortens a url using Base62 encoding.

## Design
IDs are stored in memory which is not ideal, using a database (e.g. SQLite, Redis, etc.) may speed up performance

## Test Cases
- Test #1: Generate mass URLs and shorten and expand to check for correctness and collisions
- Test #2: Test for invalid strings such as invalid URIs or non http/https schemes
- Test #3: Test providing the same url does not create a new id

## Run
Run using `dotnet run`

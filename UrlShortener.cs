namespace ShortUrl;

/// <summary>
/// A URL Shortener using Base62.
/// </summary>
public class UrlShortener
{
    /// <summary>
    /// The Base62 chars.
    /// </summary>
    private const string Base62Chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    /// <summary>
    /// A char-index map.
    /// </summary>
    private static readonly Dictionary<char, int> charToIndexMap = new Dictionary<char, int>();

    /// <summary>
    /// An id to long url map.
    /// This will not be as efficient as an indexed database but should be sufficient for demo purposes.
    /// </summary>
    private readonly Dictionary<long, string> idUrlMap = new Dictionary<long, string>();

    /// <summary>
    /// UrlShortener constructor.
    /// Populates the char-index map.
    /// </summary>
    static UrlShortener()
    {
        for (int i = 0; i < Base62Chars.Length; i++)
        {
            charToIndexMap[Base62Chars[i]] = i;
        }
    }

    /// <summary>
    /// Shortens a long URL.
    /// </summary>
    /// <param name="longUrl">The long url to shorten.</param>
    /// <returns>The short url path.</returns>
    public string ShortenUrl(string longUrl)
    {
        // We only want http and https schemes
        bool result = Uri.TryCreate(longUrl, UriKind.Absolute, out Uri? uriResult);
        if (result && (uriResult!.Scheme == Uri.UriSchemeHttp || uriResult!.Scheme == Uri.UriSchemeHttps))
        {
            long id = GetIdFromUrl(longUrl);
            return EncodeBase62(id);
        }
        else
        {
            throw new Exception("URL is not valid");
        }
    }

    /// <summary>
    /// Expands a short url.
    /// </summary>
    /// <param name="shortUrl">The short url.</param>
    /// <returns>The full expanded url.</returns>
    public string ExpandUrl(string shortUrl)
    {
        long id = DecodeBase62(shortUrl);
        return GetUrlFromId(id);
    }

    /// <summary>
    /// Encodes a long into base62.
    /// </summary>
    /// <param name="number">The number to encode.</param>
    /// <returns>The base62 string.</returns>
    private string EncodeBase62(long number)
    {
        string result = "";

        do
        {
            int remainder = (int)(number % 62);
            result = Base62Chars[remainder] + result;
            number /= 62;
        }
        while (number > 0);

        return result;
    }

    /// <summary>
    /// Decodes a base62 string to a long number.
    /// </summary>
    /// <param name="str">The base62 string.</param>
    /// <returns>The long number.</returns>
    private long DecodeBase62(string str)
    {
        long result = 0;

        for (int i = 0; i < str.Length; i++)
        {
            result = result * 62 + charToIndexMap[str[i]];
        }
        
        return result;
    }

    /// <summary>
    /// Get the long url from an id.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <returns>The long url.</returns>
    private string GetUrlFromId(long id)
    {
        if (idUrlMap.ContainsKey(id))
        {
            return idUrlMap[id];
        }
        else
        {
            throw new Exception("Long URL not found");
        }
    }

    /// <summary>
    /// Get the id from a long url.
    /// </summary>
    /// <param name="url">The long url.</param>
    /// <returns>The id.</returns>
    private long GetIdFromUrl(string url)
    {
        if (idUrlMap.ContainsValue(url))
        {
            return idUrlMap.FirstOrDefault(kv => kv.Value == url).Key;
        }
        else
        {
            long key = idUrlMap.Count;
            idUrlMap[key] = url;
            return key;
        }
    }
}
